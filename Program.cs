
using jwtTokens;

var builder = WebApplication.CreateBuilder(args);
Registration.ConfigureServices(builder.Services, builder.Configuration);

var app = builder.Build();


// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

app.UseDeveloperExceptionPage();
 
app.UseDefaultFiles();
app.UseStaticFiles();
 
app.UseRouting();
 
app.UseAuthentication();
app.UseAuthorization();
 
app.UseEndpoints(endpoints =>{ endpoints.MapDefaultControllerRoute();});

//app.UseHttpsRedirection();

//app.UseAuthorization();

//app.MapControllers();

app.Run();
