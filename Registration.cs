﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace jwtTokens;

public static class Registration
{
    public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddControllers();
        //services.AddEndpointsApiExplorer();
        //services.AddSwaggerGen();

        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            // укзывает, будет ли валидироваться издатель при валидации токена
                            ValidateIssuer = true,
                            // строка, представляющая издателя
                            ValidIssuer = AuthOptions.ISSUER,
 
                            // будет ли валидироваться потребитель токена
                            ValidateAudience = true,
                            // установка потребителя токена
                            ValidAudience = AuthOptions.AUDIENCE,
                            // будет ли валидироваться время существования
                            ValidateLifetime = true,
 
                            // установка ключа безопасности
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                            // валидация ключа безопасности
                            ValidateIssuerSigningKey = true,
                        };
                    });
        
        services.AddControllersWithViews();


        //services.AddDbContext<IdentityContext>(opt => opt.UseNpgsql(configuration.GetConnectionString("ModuleDatabase"),
        //    x => x.MigrationsHistoryTable("__MigrationHistory", "identity")));
        //var mapperConfig = new MapperConfiguration(mc =>
        //{
        //    mc.AddProfile(new MappingProfile());
        //});
        //IMapper mapper = mapperConfig.CreateMapper();
        //services.AddSingleton(mapper);

        //services.AddTransient<IUserRepository, UserRepository>();
        
        
        ////При старте приложения запускаем миграции
        //using var serviceProvider = services.BuildServiceProvider();
        //using var context = serviceProvider.GetRequiredService<IdentityContext>();
        //context.Database.Migrate();
    }
}
